from prometheus_client import start_http_server, Counter, Gauge
import argparse
import subprocess
import re
from select import select
import logging
logger = logging.getLogger(__name__)

metrics = {
    "timestamp": Gauge("radeontop_last_scrape", "Timestamp of last radeontop info", ["bus"], unit="timestamp"),
    "gpu": Gauge("radeontop_gpu", "Graphics pipe utilization", ["bus"], unit="percent"),
    "ee": Gauge("radeontop_ee", "Event engine utilization", ["bus"], unit="percent"),
    "vgt": Gauge("radeontop_vgt", "Vertex grouper + tesselator utilization", ["bus"], unit="percent"),
    "ta": Gauge("radeontop_ta", "Texture addresser utilization", ["bus"], unit="percent"),
    "tc": Gauge("radeontop_tc", "Texture cache utilization", ["bus"], unit="percent"),
    "sx": Gauge("radeontop_sx", "Shader export", ["bus"], unit="percent"),
    "sh": Gauge("radeontop_sh", "Sequencer instruction cache", ["bus"], unit="percent"),
    "spi": Gauge("radeontop_spi", "Shader interpolator", ["bus"], unit="percent"),
    "smx": Gauge("radeontop_smx", "Shader memory exchange", ["bus"], unit="percent"),
    "cr": Gauge("radeontop_cr", "Clip rectangle", ["bus"], unit="percent"),
    "sc": Gauge("radeontop_sc", "Scan converter", ["bus"], unit="percent"),
    "pa": Gauge("radeontop_pa", "Primitive assembly", ["bus"], unit="percent"),
    "db": Gauge("radeontop_db", "Depth block", ["bus"], unit="percent"),
    "cb": Gauge("radeontop_cb", "Color block", ["bus"], unit="percent"),
    "vram": Gauge("radeontop_vram", "Video memory utilization", ["bus"], unit="percent"),
    "vram_mb": Gauge("radeontop_vram", "Video memory usage", ["bus"], unit="mb"),
    "gtt": Gauge("radeontop_gtt", "GTT memory utilization", ["bus"], unit="percent"),
    "gtt_mb": Gauge("radeontop_gtt", "GTT memory usage", ["bus"], unit="mb"),
    "mclk": Gauge("radeontop_mclk", "Max memory clock utilization", ["bus"], unit="percent"),
    "mclk_ghz": Gauge("radeontop_mclk", "Memory clock", ["bus"], unit="ghz"),
    "sclk": Gauge("radeontop_sclk", "Max shader clock utilization", ["bus"], unit="percent"),
    "sclk_ghz": Gauge("radeontop_sclk", "Shader clock", ["bus"], unit="ghz"),
}


def main():
    parser = argparse.ArgumentParser(prog="Prometheus radeontop exporter",
                                     description="Export radeontop GPU metrics for prometheus")
    parser.add_argument('-b', '--bind', default="0.0.0.0", type=str,
                        help="IP adress to bind the listener on")
    parser.add_argument('-p', '--port', default=8123, type=int,
                        help="The port the exporter will listen on for requests from prometheus")
    parser.add_argument('-i', '--interval', default=15, type=int,
                        help="How often to collect metrics from the GPU (in seconds)")
    parser.add_argument('--debug', action="store_true", default=False,
                        help="Print debug output to the console")
    args = parser.parse_args()
    
    if args.debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', encoding='utf-8', level=level)

    start_http_server(args.port, args.bind)

    def detect_gpus():
        lspci = subprocess.run(['lspci'], capture_output=True)
        return re.findall("([0-9a-d]{2}).*VGA", lspci.stdout.decode('utf-8'))

    gpus = detect_gpus()
    gpus = ['03', '6c']

    logger.info(f"Found GPUs on buses {gpus}")

    radeontop_procs = [
        subprocess.Popen(
            ['radeontop', '-d', '-', '-i', str(args.interval), '--bus', str(gpu)], stdout=subprocess.PIPE
        ) for gpu in gpus
    ]

    logger.info(f"Processes: {[proc.args for proc in radeontop_procs]}")

    def get_pattern(out_id, pattern):
        return f"(?P<{out_id}>{pattern})"

    def get_metric_pattern(in_id, out_id=None, out_id2=None, pattern="[0-9]+\.[0-9]+"):
        if out_id is None:
            out_id = in_id

        pattern_string = f"{in_id} {get_pattern(out_id, pattern)}"

        if out_id2 is not None:
            pattern_string += f"[^0-9]*{get_pattern(out_id2, pattern)}"

        return pattern_string.lstrip()

    metric_patterns = [
        ["", "timestamp"],
        ["bus", "bus", None, "[^\s,]*"],
        ["gpu"],
        ["ee"],
        ["vgt"],
        ["ta"],
        ["tc"],
        ["sx"],
        ["sh"],
        ["spi"],
        ["smx"],
        ["cr"],
        ["sc"],
        ["pa"],
        ["db"],
        ["cb"],
        ["vram", "vram", "vram_mb"],
        ["gtt", "gtt", "gtt_mb"],
        ["mclk", "mclk", "mclk_ghz"],
        ["sclk", "sclk", "sclk_ghz"],
    ]

    pattern_strings = [get_metric_pattern(*metric_pattern) for metric_pattern in metric_patterns]
    patterns = [re.compile(pattern_string)for pattern_string in pattern_strings]
    logger.debug(f"Patterns: {patterns}")

    logger.info(f"Streams: {[radeontop.stdout for radeontop in radeontop_procs]}")

    while True:
        streams = select([radeontop.stdout for radeontop in radeontop_procs], [], [])
        logger.debug(f"Streams with data: {streams}")
        for stream in streams[0]:
            line = stream.readline()
            matchdict = {}
            line = line.decode('utf-8').strip()
            logger.debug(f"line: {line}")

            for pattern in patterns:
                match = pattern.search(line)
                if match is not None:
                    matchdict.update(match.groupdict())

            logger.debug(matchdict)

            if "bus" in matchdict:
                bus = matchdict.pop("bus")

                for key, value in matchdict.items():
                    logger.debug(f"Setting {metrics[key]} with bus {bus} to {float(value)}")
                    metrics[key].labels(bus=bus).set(float(value))



if __name__ == '__main__':
    main()
