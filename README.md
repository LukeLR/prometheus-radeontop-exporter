# prometheus-radeontop-exporter

A simple Python script to export radeontop AMD GPU metrics for prometheus.

## Dependencies
- Python >= 3
- `prometheus_client` Python library
- `radeontop`

## Installation
1. Clone this repository:
   ```
   git clone https://codeberg.org/LukeLR/prometheus-radeontop-exporter
   ```
2. `cd` into the cloned folder:
   ```
   cd prometheus-radeontop-exporter
   ```
3. Install the python package into your system Python library:
   ```
   sudo pip install .
   ```
4. Install the systemd service:
   ```
   sudo cp -a prometheus-radeontop-exporter.service /etc/systemd/system/
   sudo systemctl daemon-reload
   ```
5. Start and enable the systemd service:
   ```
   sudo systemctl enable --now prometheus-radeontop-exporter
   ```
6. Point your prometheus to `ip:8123`:
   ```
   scrape_configs:
     - job_name: "radeontop"
       static_configs:
         - targets:
           - 'ip:8123'
    ```
7. Enjoy!

## Grafana dashboard
An example grafana dashboard is available in [grafana-example-dashboard.json](grafana-example-dashboard.json). It looks somewhat like this:
![Grafana dashboard screenshot 01](grafana-example-dashboard01.png)
![Grafana dashboard screenshot 02](grafana-example-dashboard02.png)
![Grafana dashboard screenshot 03](grafana-example-dashboard03.png)
![Grafana dashboard screenshot 04](grafana-example-dashboard04.png)
![Grafana dashboard screenshot 05](grafana-example-dashboard05.png)
![Grafana dashboard screenshot 06](grafana-example-dashboard06.png)
